<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;

/**
* Implements hook_form_alter
*/
function posse_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'entity_view_display_edit_form') {
    if ( \Drupal::entityTypeManager()->getDefinition($form['#entity_type']) instanceof \Drupal\Core\Entity\ContentEntityType ) {
      $manager = \Drupal::service('plugin.manager.posse');
      if (!empty($manager->getDefinitions())) {
        $form['posse'] = [
          '#type' => 'details',
          '#title' => t('POSSE Settings'),
          '#description' => t('Manage how this content is syndicated.')
        ];
        foreach($manager->getDefinitions() as $id => $definition) {
          $plugin = $manager->createInstance($id, []);
          $form['posse'][$id] = [
            '#type' => 'details',
            '#title' => $definition['label'],
            '#tree' => TRUE,
          ];
          $form['posse'][$id]['settings'] = $plugin->syndicateForm($form, $form_state);
          if (empty($form['posse'][$id]['settings'])) {
            unset($form['posse'][$id]);
          }
          if (!in_array("posse_syndicate_settings_validate", $form['#validate'])) {

             array_unshift($form['#validate'], "posse_syndicate_settings_validate");
             array_unshift($form['actions']['submit']['#submit'], "posse_syndicate_settings_submit");
          }
        }
      }
    }
  }
}

/**
* Calls on the plugin's validation handlers.
*/
function posse_syndicate_settings_validate($form, FormStateInterface &$form_state) {
  $manager = \Drupal::service('plugin.manager.posse');
  if (!empty($manager->getDefinitions())) {
    foreach($manager->getDefinitions() as $id => $definition) {
      $plugin = $manager->createInstance($id, []);
      $plugin->syndicateFormValidate($form, $form_state);
    }
  }
}

/**
* Calls on the plugin's submit handlers.
*/
function posse_syndicate_settings_submit($form, FormStateInterface $form_state) {
  $manager = \Drupal::service('plugin.manager.posse');
  if (!empty($manager->getDefinitions())) {
    foreach($manager->getDefinitions() as $id => $definition) {
      $plugin = $manager->createInstance($id, []);
      $plugin->syndicateFormSubmit($form, $form_state);
    }
  }
}

/**
* Implements hook_entity_insert
*/
function posse_entity_insert(EntityInterface $entity) {
  $manager = \Drupal::service('plugin.manager.posse');
  if (\Drupal::entityTypeManager()->getDefinition($entity->getEntityTypeId()) instanceof \Drupal\Core\Entity\ContentEntityType) {
    if (!empty($manager->getDefinitions())) {
      foreach($manager->getDefinitions() as $id => $definition) {
        $plugin = $manager->createInstance($id, []);
        $plugin->syndicate($entity, TRUE);
      }
    }
  }
}

/**
* Implements hook_entity_update
*/
function posse_entity_update(EntityInterface $entity) {
  $manager = \Drupal::service('plugin.manager.posse');
  if (\Drupal::entityTypeManager()->getDefinition($entity->getEntityTypeId()) instanceof \Drupal\Core\Entity\ContentEntityType) {
    if (!empty($manager->getDefinitions())) {
      foreach($manager->getDefinitions() as $id => $definition) {
        $plugin = $manager->createInstance($id, []);
        $plugin->syndicate($entity, FALSE);
      }
    }
  }
}
